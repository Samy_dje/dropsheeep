<!DOCTYPE html>
<html>

<head>
    <!-- squelette html -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- Mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <!-- Site Metas -->
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="shortcut icon" href="images/fevicon.png" type="image/x-icon">
    <title>DropSheep</title>

    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <!--------------------------------------------------------------------------------------------------------->

    <!-- fonts style (texts) -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@700&display=swap" rel="stylesheet">

    <!--------------------------------------------------------------------------------------------------------->
    <!-- nice select stylesheet -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css"
          integrity="sha256-mLBIhmBvigTFWPSCtvdu6a76T+3Xyt+K571hupeFLg4=" crossorigin="anonymous"/>


    <!--------------------------------------------------------------------------------------------------------->


    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet"/>
    <!-- responsive style -->
    <link href="css/responsive.css" rel="stylesheet"/>
</head>

<body>
<div class="hero_area">
    <div class="hero_bg_box">
        <img id="hero_image" src="images/dalle1.webp" alt="">
    </div>
    <!-- header section strats -->
    <header class="header_section">
        <div class="header_bottom">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg custom_nav-container ">
                    <a class="navbar-brand " href="index.php"> DropSheep </a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class=""> </span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav  ">
                            <li class="nav-item active">
                                <a class="nav-link" href="index.php"><i class="fa fa-home" aria-hidden="true"></i> Home
                                    <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="service.php"><i class="fa-solid fa-laptop"></i> Produits </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact.php"><i
                                        class="fa-solid fa-phone"></i>Contactez-nous</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="about.php"> <i class="fa-solid fa-info"></i> À propos</a>
                            </li>
                            <?php
                            session_start();
                            // Vérifiez si l'utilisateur est connecté
                            if (isset($_SESSION['User_ID'])) {
                                // L'utilisateur est connecté, affichez le lien vers la page du profil
                                echo '<li class="nav-item"><a class="nav-link" href="profil.php"><i class="fa fa-user" aria-hidden="true"></i></a></li>';
                                echo '<li class="nav-item"><a class="nav-link" href="deconnexion.php"><i class="fa fa-user" aria-hidden="true"></i><span>Se Déconnecter</span></a></li>';
                            } else {
                                // L'utilisateur n'est pas connecté, affichez "Se connecter" et "S'enregistrer"
                                echo '<li class="nav-item"><a class="nav-link" href="connexion.php"><i class="fa fa-user" aria-hidden="true"></i><span>Se Connecter</span></a></li>';
                                echo '<li class="nav-item"><a class="nav-link" href="inscription.php"><i class="fa fa-user" aria-hidden="true"></i><span>S\'enregistrer</span></a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <!-- about section -->
    <!-- about section -->
    <section class="about_section layout_padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5 offset-md-1">
                    <div class="detail-box pr-md-2">
                        <div class="heading_container">
                            <h2>À propos</h2>
                        </div>
                        <p class="mt-3">
                            DropSheep est votre destination incontournable pour le dropshipping sans tracas. Notre
                            mission est de fournir une plateforme transparente où les entrepreneurs peuvent se connecter
                            avec des fournisseurs fiables, offrant une large gamme de produits. Nous croyons en la
                            simplification du processus de dropshipping, permettant aux entreprises de prospérer dans le
                            paysage concurrentiel du commerce électronique.
                            </br>
                            Avec un engagement envers la qualité et la satisfaction client, DropSheep se distingue en
                            tant que partenaire de confiance pour les entrepreneurs en herbe et établis. Explorez le
                            monde des possibilités avec DropSheep, où votre succès est notre priorité.
                        </p>
                        <a href="about.php" class="btn btn-primary mt-3">Read More</a>
                    </div>
                </div>
                <div class="col-md-6 px-0">
                    <div class="img-box">
                        <img src="images/about_2.webp" alt="about img" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- about section ends -->

    <!-- about section ends -->

</div>

<footer>
    <section class="info_section">
        <div class="container">
            <div class="info_top">
                <div class="row">
                    <div class="col-md-3">
                        <a class="navbar-brand" href="index.html">
                            DropSheep
                        </a>
                    </div>
                    <div class="col-md-5">
                        <div class="info_contact">
                            <a href="">
                                <i class="fa fa-map-marker-alt" aria-hidden="true"></i>
                                <span>Localisation</span>
                            </a>
                            <a href="">
                                <i class="fa fa-phone-alt" aria-hidden="true"></i>
                                <span>+33 123456789</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="social_box">
                            <a href="">
                                <i class="fab fa-facebook-f" aria-hidden="true"></i>
                            </a>
                            <a href="">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                            </a>
                            <a href="">
                                <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                            </a>
                            <a href="">
                                <i class="fab fa-instagram" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>

<!-- jQuery (necessaire pour nice select) -->

<script src="js/jquery-3.4.1.min.js"></script>
<!-- popper js -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.bundle.min.js"></script>
<!-- custom js -->
<script src="js/custom.js"></script>
<!-- nice select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"
        integrity="sha256-Zr3vByTlMGQhvMfgkQ5BtWRSKBGa2QlspKYJnkjZTmo=" crossorigin="anonymous"></script>
<!-- fontawesome Script (Icons) -->
<script src="https://kit.fontawesome.com/db9d3f8a27.js" crossorigin="anonymous"></script>


</body>

</html>