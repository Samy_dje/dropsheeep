-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 11 jan. 2024 à 16:13
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `dropsheep`
--
CREATE DATABASE IF NOT EXISTS `dropsheep` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `dropsheep`;

-- --------------------------------------------------------

--
-- Structure de la table `achats`
--

DROP TABLE IF EXISTS `achats`;
CREATE TABLE IF NOT EXISTS `achats`
(
    `user_id`    int NOT NULL,
    `product_id` int NOT NULL,
    `date_achat` datetime DEFAULT NULL,
    `quantite`   int      DEFAULT NULL,
    PRIMARY KEY (`user_id`, `product_id`),
    KEY `product_id` (`product_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pj_user`
--

DROP TABLE IF EXISTS `pj_user`;
CREATE TABLE IF NOT EXISTS `pj_user`
(
    `User_ID`           int         NOT NULL AUTO_INCREMENT,
    `User_Pseudo`       varchar(30) NOT NULL,
    `User_MDP`          varchar(30) NOT NULL,
    `User_Bio`          varchar(255)         DEFAULT NULL,
    `User_Adresse`      varchar(50) NOT NULL,
    `User_Role`         varchar(20)          DEFAULT NULL,
    `User_Points`       int         NOT NULL DEFAULT '0',
    `User_Classement_M` int                  DEFAULT NULL,
    `User_Classement_S` int                  DEFAULT NULL,
    `User_Classement_J` int                  DEFAULT NULL,
    `User_Vehicule`     binary(2)            DEFAULT NULL,
    `User_Image`        varchar(255)         DEFAULT NULL,
    PRIMARY KEY (`User_ID`),
    UNIQUE KEY `User_Pseudo` (`User_Pseudo`),
    UNIQUE KEY `User_Classement_J` (`User_Classement_J`),
    UNIQUE KEY `User_Classement_S` (`User_Classement_S`),
    UNIQUE KEY `User_Classement_M` (`User_Classement_M`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 14
  DEFAULT CHARSET = latin1;

--
-- Déchargement des données de la table `pj_user`
--

INSERT INTO `pj_user` (`User_ID`, `User_Pseudo`, `User_MDP`, `User_Bio`, `User_Adresse`, `User_Role`, `User_Points`,
                       `User_Classement_M`, `User_Classement_S`, `User_Classement_J`, `User_Vehicule`, `User_Image`)
VALUES (10, 'samy', 'sasa', 'sqfqsf', 'fsqfqs', 'rien', 0, NULL, NULL, NULL, NULL, NULL),
       (11, 'karim', 'asia', 'sqdf', 'qsf', 'qsfg', 0, NULL, NULL, NULL, NULL, NULL),
       (12, 'cyril ', '123', 'sdgdsg', 'sdgsdg', 'sdgsd', 0, NULL, NULL, NULL, NULL, NULL),
       (13, 'rahima', 'rahim', 'yes', '36 Rue Boris Vian', 'rien', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products`
(
    `id`           int            NOT NULL AUTO_INCREMENT,
    `nom_produits` varchar(255)   NOT NULL,
    `prix`         decimal(10, 2) NOT NULL,
    `description`  text,
    `en_stock`     tinyint(1)     NOT NULL,
    `image`        varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 24
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `nom_produits`, `prix`, `description`, `en_stock`, `image`)
VALUES (1, 'Modèle X Pro', 299.00, 'Description', 10, 'images\\Acceuil-background.jpg'),
       (2, 'Creative Pen', 299.00, 'Description', 5, 'images\\CreativePen.jpg'),
       (3, 'Immersion 360°', 299.00, 'Description', 12, 'images\\vr2.jpg'),
       (4, 'Health Pro', 299.00, 'Description', 12, 'images\\Health.jfif'),
       (5, 'Routeur Wi-Fi 6 Ultra-Speed', 299.00, 'Description', 14, 'images\\routeur.jfif'),
       (7, 'Projecteur Magcubique Hy300 4K Android 11 Touriste', 100.00,
        'Prise en charge du décodage et de la lecture vidéo 4K, la sortie est jusqu\'à 1280*720p180 ° flexible', 10,
        'https://ae01.alicdn.com/kf/Se60bac2d087e4ea2911cd368ed1992c6G/Projecteur-Magcubique-Hy300-4K-Android-11-Touriste-Wifi-6-200-ANSI-Allwinner-H713-BTpig-1080P-1280.jpg_.webp'),
       (9, 'Montre intelligente militaire C20 pour homme', 50.00,
        'Podomètre, Sommeil, Fréquence cardiaque, Entraînement, Pression artérielle, Météo, Contrôle de la caméra, Oxygène sanguin, Contrôle de la musique...\r\n',
        4,
        'https://ae01.alicdn.com/kf/S3dca39ce84814dd88e1dc85939c40006L/Montre-intelligente-militaire-C20-pour-homme-noir-carbone-ultra-arm-e-ext-rieur-IP68-tanche-fr.jpg_640x640.jpg_.webp'),
       (10, 'Console de jeu vidéo rétro R36S Pro', 80.00,
        'La console de jeu vidéo portable rétro Open Source R36S mise à jour, l\'expérience de jeu ultime pour les joueurs en déplacement! ...',
        0,
        'https://ae01.alicdn.com/kf/S5459ba4daec04406bece0591b1e9cd8cn/Console-de-jeu-vid-o-r-tro-R36S-Pro-syst-me-Linux-cran-IPS-3-5.jpg_.webp'),
       (11, 'Montre intelligente Huawei GT4 Pro pour homme', 50.00,
        'ntraînement respiratoire, Cadran poussoir, Chronomètre, Réglage de la luminosité, Plusieurs modes de sport, Suivi du sommeil...',
        4,
        'https://ae01.alicdn.com/kf/S324a4ccf3e11425ba96b4e8c227b1505J/Montre-intelligente-Huawei-GT4-Pro-pour-homme-cran-HD-AMOLED-appel-Bluetooth-GPS-NDavid-fr-quence.jpg_640x640.jpg_.webp'),
       (12, 'Drone pliable avec caméra HD grand angle pour touristes', 60.00,
        'Nouveau E88Pro RC Drone 4K professionnel avec 1080P grand Angle HD caméra pliable RC hélicoptère WIFI FPV hauteur tenir cadeau jouet',
        12,
        'https://ae01.alicdn.com/kf/Sc847331c59de4138bfdb293f3613bfc7x/Drone-pliable-avec-cam-ra-HD-grand-angle-pour-touristes-h-licopt-re-RC-4K-professionnel.jpg_640x640.jpg_.webp'),
       (13, 'Andoer-Lumière vidéo LED RVB', 50.00,
        'Andoer W140 RGB LED Lumière vidéo Photographie Lumière de remplissage CRI95 + 2500-9000K LCD Display Cold Shoe pour Vlog Live Streaming Video',
        12,
        'https://ae01.alicdn.com/kf/H37e806fbd61547a38732ea42f49182ceM/Andoer-Lumi-re-vid-o-LED-RVB-lumi-re-de-remplissage-de-photographie-CRI95-cran.jpg_640x640.jpg_.webp'),
       (14, 'Écouteurs filaires', 20.00,
        'pour Xiaomi, 14, 13, 12, 11 Pro, Ultra Lite, 3.5mm, Redmi, POCO, Huawei, Samsung, écouteurs, accessoires', 6,
        'https://ae01.alicdn.com/kf/S1eaf7a14cb8c4d35813bf3eeb35b10a0J/couteurs-filaires-de-type-C-pour-Xiaomi-14-13-12-11-Pro-Ultra-Lite-3.png_.webp'),
       (15, 'Écouteurs Bluetooth sans fil, bande de sommeil', 10.00,
        'casque de musique, élastique doux, confortable, sport, sauna, téléphone', 7,
        'https://ae01.alicdn.com/kf/Sba74b1b483d84cb190e095ae55222652s/couteurs-Bluetooth-sans-fil-bande-de-sommeil-casque-de-musique-lastique-doux-confortable-sport-sauna.jpg_640x640.jpg_.webp'),
       (16, 'Console de jeux vidéo rétro U9 PRO', 80.00,
        'Console de jeu TV sans fil U9 PRO HDMI HD 2.4G 3D God of War PSP Tekken 6 P5, ', 7,
        'https://ae01.alicdn.com/kf/S4114ab88a8f749df944e025d9127f0b5R/Console-de-jeux-vid-o-r-tro-U9-PRO-HDMI-haute-d-finition-2-4G-sans.jpg_640x640.jpg_.webp'),
       (17, 'Console de jeu rétro classique pour adultes et enfants', 50.00,
        'Console de jeu rétro Mini système de jeu vidéo classique 620 jeux intégrés Console TV 8 bits FC Nes pour adultes et enfants',
        9,
        'https://ae01.alicdn.com/kf/S772b94fac3364959bdb30e4592bfd89ar/Console-de-jeu-r-tro-classique-pour-adultes-et-enfants-mini-syst-me-de-jeu-vid.jpg_.webp'),
       (18, 'Écouteurs sans fil Bluetooth Fone', 30.00,
        'écouteurs Bluetooth en forme de sport bandeau de couchage casque élastique sans fil masque pour les yeux casque sans fil Bluetooth bandeau',
        5,
        'https://ae01.alicdn.com/kf/S38681f88f26f44d4a8be30d74d2e43c1L/couteurs-sans-fil-Bluetooth-Fone-casque-lastique-masque-pour-les-yeux-de-musique-bande-de.jpg_640x640.jpg_.webp'),
       (19, 'AROMA 3 en 1 AMT-600 JETuner', 0.00, 'Metronome/Tuner/Générateur de Tonalité DNomatic Métal Shell', 4,
        'https://ae01.alicdn.com/kf/S89d75f80af9448bab6ec1cf61d9ebf8eZ/AROMA-3-en-1-AMT-600-JETuner-Pas-Effet-P-dale-Metronome-Tuner-G-n-rateur.jpg_.webp'),
       (20, 'Tanix-Boîtier TV W2, Android 11, Amlogic S905W2,', 100.00,
        'Nouveau Tanix W2 Android 11 Smart TV box Amlogic S905W2 2GB 16GB 2.4G/5G double Wifi BT décodeur lecteur multimédia PK Tanix TX3',
        12,
        'https://ae01.alicdn.com/kf/S8d3dd27089d3481bb1117c0b46fe1cc7H/Tanix-Bo-tier-TV-W2-Android-11-Amlogic-S905W2-2-Go-16-Go-AV1-BT-2.jpg_640x640.jpg_.webp'),
       (21, 'M17-Console de jeu vidéo portable', 80.00,
        'Console de jeu vidéo rétro M17 4.3 pouces 480*272 écran LCD Mini console de jeu vidéo portable Emuelec jeu intégré 25 émulateurs.',
        12,
        'https://ae01.alicdn.com/kf/S5532b608e1734fc4bd77007e1e66ed561/M17-Console-de-jeu-vid-o-portable-console-de-jeu-r-tro-64-Go-128-Go.jpg_640x640.jpg_.webp'),
       (22, 'Mini console de jeu vidéo portable rétro pour enfants', 60.00,
        'Avec 500 jeux intégrés, vous ne manquerez jamais d\'options pour des heures de divertissement sans fin.', 14,
        'https://ae01.alicdn.com/kf/Se3f1f93b56e84fce8e71d396ee6cc819a/Mini-console-de-jeu-vid-o-portable-r-tro-pour-enfants-lecteur-de-jeu-document-LCD.jpg_.webp'),
       (23, 'Lenovo-Écouteurs intra-auriculaires sans fil GM2 Pro', 30.00,
        'Écouteurs Lenovo GM2 Pro Bluetooth 5.3 casque de sport sans fil de jeu intra-auriculaire casque de musique double mode à faible latence nouveau.',
        19,
        'https://ae01.alicdn.com/kf/Sb549229b319b40c3871cff6d8073a4b47/Lenovo-couteurs-intra-auriculaires-sans-fil-GM2-Pro-Bluetooth-5-3-casque-de-sport-casque-de.jpg_.webp');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `achats`
--
ALTER TABLE `achats`
    ADD CONSTRAINT `achats_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `pj_user` (`User_ID`),
    ADD CONSTRAINT `achats_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
