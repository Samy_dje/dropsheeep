$(document).ready(function () {
    // Sélection des éléments avec la classe 'card' et application d'un gestionnaire d'événement de survol.
    $('.card').hover(
        // Première fonction : exécutée lors du survol de la souris sur l'élément 'card'.
        function () {
            // Récupération de la valeur de l'attribut 'data-bg-img' de l'élément survolé.
            // Cette valeur est supposée être un chemin vers une nouvelle image.
            var newImage = $(this).data('bg-img');

            // Modification de l'attribut 'src' de l'élément d'image avec l'ID 'hero_image' pour utiliser 'newImage'.
            // Cela change l'image affichée dans l'élément 'hero_image'.
            $('#hero_image').attr('src', newImage);
        },
        // Deuxième fonction : exécutée lors de la sortie de la souris de l'élément 'card'.
        function () {
            // Réinitialisation de l'attribut 'src' de l'élément 'hero_image' à son chemin d'image par défaut.
            $('#hero_image').attr('src', 'images/dalle1.webp');
        }
    );
});

function searchProduct() {
    var input = document.getElementById("product-search");
    var filter = input.value.toUpperCase();
    var cards = document.getElementsByClassName("card");

    // Effacer tout surlignage précédent
    var highlightedTitles = document.querySelectorAll(".card-body h5.card-title.highlighted");
    highlightedTitles.forEach(function (title) {
        title.classList.remove("highlighted");
        title.style.backgroundColor = ""; // Enlevez le surlignage jaune
    });

    // Si rien n'est écrit, ne rien faire d'autre
    if (!filter.trim()) {
        return; // Sortir de la fonction si la recherche est vide
    }

    // Recherche et surlignage du produit
    var found = false;
    for (var i = 0; i < cards.length; i++) {
        var title = cards[i].querySelector(".card-body h5.card-title");
        if (title && title.innerHTML.toUpperCase().indexOf(filter) > -1) {
            title.classList.add("highlighted"); // Ajoutez une classe pour le surlignage
            title.style.backgroundColor = "yellow"; // Appliquez le surlignage jaune
            // Défiler vers le produit
            cards[i].scrollIntoView({behavior: "smooth", block: "center"});
            found = true;
            break;
        }
    }

    // Si aucun produit n'a été trouvé
    if (!found && filter) {
        alert("Produit non trouvé.");
    }
}

$(document).ready(function () {
    $('select').niceSelect();
    // Cette fonction est fournie par le plugin jQuery Nice Select.
    // Elle remplace les éléments <select> standards par des versions personnalisées
    // qui sont plus esthétiques et potentiellement plus conviviales.
});

/* Panneau de mon panier*/

/*Ouverture et la fermeture du panneau*/
document.addEventListener('DOMContentLoaded', function () {
    // Sélectionner l'icône du panier et le panneau du panier
    var cartIcon = document.querySelector('.icon-cart');
    var cartTab = document.querySelector('.cartTab');
    var closeButton = document.querySelector('.cartTab .close');

    // Ajouter un écouteur d'événement pour ouvrir le panneau du panier
    cartIcon.addEventListener('click', function () {
        document.body.classList.add('showCart');
    });

    // Ajouter un écouteur d'événement pour fermer le panneau du panier
    closeButton.addEventListener('click', function () {
        document.body.classList.remove('showCart');
    });
});

/*----------------------------------------------------------------------------------------*/

/* Récuperation et manipulation des données produits depuis la BDD dropship */

let listProducts = [];


let listProductHTML = document.querySelector('.container');
let listCartHTML = document.querySelector('.listCart');
let iconCart = document.querySelector('.icon-cart');
let iconCartSpan = document.querySelector('.icon-cart span');
let cart = [];

listProductHTML.addEventListener('click', (event) => {
    let positionClick = event.target;
    if (positionClick.classList.contains('btn')) {
        // recuperer l'id de chaque produit 'data-id' dans le fichier html construit par le PHP
        let product_id = positionClick.parentElement.parentElement.dataset.id;
        addToCart(product_id)
    }
})

const addToCart = (product_id) => {
    // Recherche du produit dans le panier en utilisant son ID.
    let positionThisProductInCart = cart.findIndex((value) => value.product_id == product_id);
    // Si le panier est vide, crée un nouvel objet produit avec une quantité de 1 et l'ajoute au panier.
    if (cart.length <= 0) {
        cart = [{
            product_id: product_id,
            quantity: 1
        }];
        console.log(cart)
    } else if (positionThisProductInCart < 0) {
        cart.push({
            product_id: product_id,
            quantity: 1
        });
        console.log(cart)

    } else {
        cart[positionThisProductInCart].quantity = cart[positionThisProductInCart].quantity + 1;
        console.log(cart)
    }
    // Appelle une fonction (non définie ici) pour mettre à jour l'affichage du panier dans l'interface utilisateur.
    addCartToHTML();
}

const addCartToHTML = () => {
    // Réinitialise le contenu HTML de la liste du panier.
    listCartHTML.innerHTML = '';
    // Initialise un compteur pour la quantité totale d'articles dans le panier.
    let totalQuantity = 0;
    // Vérifie si le panier contient des articles.
    if (cart.length > 0) {
        cart.forEach(item => {
            // Cumule la quantité totale d'articles.
            totalQuantity = totalQuantity + item.quantity;
            // Crée un nouvel élément div pour représenter l'article dans le panier.
            let newItem = document.createElement('div');
            newItem.classList.add('item');
            newItem.dataset.id = item.product_id;
            // Trouve les informations du produit correspondant dans la liste des produits.
            let positionProduct = listProducts.findIndex((value) => value.id == item.product_id);
            let info = listProducts[positionProduct];
            listCartHTML.appendChild(newItem);
            // Définit le contenu HTML du nouvel élément, incluant les détails du produit et la quantité.
            newItem.innerHTML = `
            <div class="image">
                    <img src="${info.image}">
                </div>
                <div class="name">
                ${info.nom_produits}
                </div>
                <div class="totalPrice">$${info.prix * item.quantity}</div>
                <div class="quantity">
                    <span class="minus">-</span>
                    <span>${item.quantity}</span>
                    <span class="plus">+</span>
                </div>
            `;
        })
    }
    iconCartSpan.innerText = totalQuantity;
}
// Ajout d'un écouteur d'événements 'click' à l'élément listCartHTML.
listCartHTML.addEventListener('click', (event) => {
    let positionClick = event.target;
    // Vérifie si l'élément cliqué est un bouton 'minus' ou 'plus'.
    if (positionClick.classList.contains('minus') || positionClick.classList.contains('plus')) {
        let product_id = positionClick.parentElement.parentElement.dataset.id;
        // Détermine le type d'action (augmenter ou diminuer la quantité).
        let type = 'minus';
        if (positionClick.classList.contains('plus')) {
            type = 'plus';
        }
        // Appelle la fonction changeQuantityCart avec l'ID du produit et le type d'action.
        changeQuantityCart(product_id, type);
    }
})

// ajuster la quantité d'un produit dans le panier (identifié par cart, un tableau) et mettre à jour l'affichage du panier en conséquence
const changeQuantityCart = (product_id, type) => {
    let positionItemInCart = cart.findIndex((value) => value.product_id == product_id);
    if (positionItemInCart >= 0) {
        let info = cart[positionItemInCart];
        switch (type) {
            case 'plus':
                cart[positionItemInCart].quantity = cart[positionItemInCart].quantity + 1;
                break;

            default:
                let changeQuantity = cart[positionItemInCart].quantity - 1;
                if (changeQuantity > 0) {
                    cart[positionItemInCart].quantity = changeQuantity;
                } else {
                    cart.splice(positionItemInCart, 1);
                }
                break;
        }
    }
    addCartToHTML();
}


/* Fonction principale */
const initApp = () => {
    // get data product
    fetch('../traitement_achat.php')
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            listProducts = data;
            console.log(listProducts);
        })
        .catch(error => {
            console.error('There was a problem with the fetch operation:', error);
        });
}
initApp();
