<!DOCTYPE html>
<html>

<head>
    <!-- squelette html -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- Mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <!-- Site Metas -->
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="shortcut icon" href="images/fevicon.png" type="image/x-icon">
    <title>DropSheep</title>

    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <!--------------------------------------------------------------------------------------------------------->

    <!-- fonts style (texts) -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@700&display=swap" rel="stylesheet">

    <!--------------------------------------------------------------------------------------------------------->

    <!-- owl slider stylesheet -->
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
    <!-- nice select stylesheet -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css"
          integrity="sha256-mLBIhmBvigTFWPSCtvdu6a76T+3Xyt+K571hupeFLg4=" crossorigin="anonymous"/>


    <!--------------------------------------------------------------------------------------------------------->


    <link href="css/style.css" rel="stylesheet"/>
    <!-- responsive style -->
    <link href="css/responsive.css" rel="stylesheet"/>
</head>

<body>
<div class="hero_area">
    <div class="hero_bg_box">
        <img id="hero_image" src="images/dalle1.webp" alt="">
    </div>
    <header class="header_section">
        <div class="header_bottom">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg custom_nav-container ">
                    <a class="navbar-brand " href="index.php"> DropSheep </a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class=""> </span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav  ">
                            <li class="nav-item active">
                                <a class="nav-link" href="index.php"><i class="fa fa-home" aria-hidden="true"></i> Home
                                    <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="service.php"><i class="fa-solid fa-laptop"></i> Produits </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact.php"><i
                                        class="fa-solid fa-phone"></i>Contactez-nous</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="about.php"> <i class="fa-solid fa-info"></i> À propos</a>
                            </li>
                            <?php
                            session_start();
                            // Vérifiez si l'utilisateur est connecté
                            if (isset($_SESSION['User_ID'])) {
                                // L'utilisateur est connecté, affichez le lien vers la page du profil
                                echo '<li class="nav-item"><a class="nav-link" href="profil.php"><i class="fa fa-user" aria-hidden="true"></i></a></li>';
                                echo '<li class="nav-item"><a class="nav-link" href="deconnexion.php"><i class="fa fa-user" aria-hidden="true"></i><span>Se Déconnecter</span></a></li>';
                            } else {
                                // L'utilisateur n'est pas connecté, affichez "Se connecter" et "S'enregistrer"
                                echo '<li class="nav-item"><a class="nav-link" href="connexion.php"><i class="fa fa-user" aria-hidden="true"></i><span>Se Connecter</span></a></li>';
                                echo '<li class="nav-item"><a class="nav-link" href="inscription.php"><i class="fa fa-user" aria-hidden="true"></i><span>S\'enregistrer</span></a></li>';
                            }
                            ?>
                            <form class="form-inline justify-content-center">
                                <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </form>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="profile-container">
            <div class="profile-picture">
                <?php
                // Assurez-vous que la session est démarrée


                // Vérifiez si l'utilisateur est connecté
                if (isset($_SESSION['User_ID'])) {
                    // Récupérez l'ID de l'utilisateur connecté
                    $user_id = $_SESSION['User_ID'];

                    // Connexion à la base de données MySQL
                    $mysqli = new mysqli("localhost", "root", "root", "dropsheep");

                    // Vérification de la connexion
                    if ($mysqli->connect_error) {
                        die("La connexion à la base de données a échoué : " . $mysqli->connect_error);
                    }

                    // Récupération du pseudo de l'utilisateur
                    $result = $mysqli->query("SELECT User_Pseudo, User_Bio, User_Adresse, User_Role FROM pj_user WHERE User_ID = $user_id");
                    if ($result->num_rows > 0) {
                        $row = $result->fetch_assoc();
                        $pseudo = $row["User_Pseudo"];
                        $bio = $row["User_Bio"];
                        $adresse = $row["User_Adresse"];
                        $role = $row["User_Role"];
                    } else {
                        // Gérer le cas où l'utilisateur n'est pas trouvé
                        $pseudo = "Utilisateur inconnu";
                        $bio = $adresse = $role = "";
                    }

                    // Fermeture de la connexion à la base de données
                    $mysqli->close();
                } else {
                    // Si l'utilisateur n'est pas connecté, vous pouvez gérer cette condition ici
                    $pseudo = "Utilisateur non connecté";
                }
                ?>

                <img class="profile-picture" src="images/user.jpg" alt="Photo de profil">
            </div>

            <div class="profile-info">
                <h2 class="username"><?php echo htmlspecialchars($pseudo); ?></h2>
                <p class="bio"><?php echo 'Bio :' . htmlspecialchars($bio); ?></p>
                <p class="adresse"><?php echo "Adresse : " . htmlspecialchars($adresse); ?></p>
                <p class="role"><?php echo "Role : " . htmlspecialchars($role); ?></p>
            </div>
        </div>
    </div>
</div>
<footer>
    <section class="info_section">
        <div class="container">
            <div class="info_top">
                <div class="row">
                    <div class="col-md-3">
                        <a class="navbar-brand" href="index.html">
                            DropSheep
                        </a>
                    </div>
                    <div class="col-md-5">
                        <div class="info_contact">
                            <a href="">
                                <i class="fa fa-map-marker-alt" aria-hidden="true"></i>
                                <span>Localisation</span>
                            </a>
                            <a href="">
                                <i class="fa fa-phone-alt" aria-hidden="true"></i>
                                <span>+33 123456789</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="social_box">
                            <a href="">
                                <i class="fab fa-facebook-f" aria-hidden="true"></i>
                            </a>
                            <a href="">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                            </a>
                            <a href="">
                                <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                            </a>
                            <a href="">
                                <i class="fab fa-instagram" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>


<!-- jQuery (necessary for Owl Carousel) -->

<script src="js/jquery-3.4.1.min.js"></script>
<!-- popper js -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.bundle.min.js"></script>
<!-- custom js -->
<script src="js/custom.js"></script>
<!-- owl slider -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<!-- nice select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"
        integrity="sha256-Zr3vByTlMGQhvMfgkQ5BtWRSKBGa2QlspKYJnkjZTmo=" crossorigin="anonymous"></script>
<!-- fontawesome Script (Icons) -->
<script src="https://kit.fontawesome.com/db9d3f8a27.js" crossorigin="anonymous"></script>
</body>

</html>