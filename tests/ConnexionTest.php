<?php
use PHPUnit\Framework\TestCase;
include('../traitement_connexion.php');

// Classe de test ConnexionTest étendant la classe TestCase de PHPUnit
class ConnexionTest extends TestCase
{
    private $pdo; // une instance simulée de PDO
    private $stmt; // une instance simulée de PDOStatement

    // Méthode SetUp qui est exécutée avant chaque test
    public function SetUp():void
    {
        // Crée une simulation de l'objet PDO
        $this->pdo = $this->createMock(PDO::class);

        // Crée un mock de l'objet PDOStatement
        $this->stmt = $this->createMock(PDOStatement::class);

        // Configure le mock de PDO pour retourner le mock de PDOStatement sur la méthode 'prepare'
        $this->pdo->method('prepare')->willReturn($this->stmt);

        // Configure le mock de PDOStatement pour retourner true sur la méthode 'execute'
        $this->stmt->method('execute')->willReturn(true);
    }

    // Test pour vérifier le succès de l'authentification d'un utilisateur
    public function testAuthenticateUserSuccess()
    {
        $this->stmt->method('fetch')->willReturn([
            'User_ID' => '1',
            'User_Pseudo' => 'testuser',
            'User_MDP' => 'password123'
        ]);

        // Appelle la fonction authenticateUser et passe les paramètres de test
        $result = authenticateUser($this->pdo, 'testuser', 'password123');

        // Effectue des assertions pour vérifier que les résultats sont conformes aux attentes
        $this->assertIsArray($result, "Authentication should return an array.");
        $this->assertEquals('1', $result['User_ID'], "User ID should match.");
        $this->assertEquals('testuser', $result['User_Pseudo'], "User pseudo should match.");
        $this->assertEquals('password123', $result['User_MDP'], "Password should match");
    }

    // Test pour vérifier le cas où un utilisateur est introuvable
    public function testAuthenticateUserNotFound()
    {
        $this->stmt->method('fetch')->willReturn(false);

        $result = authenticateUser($this->pdo, 'unknownuser', 'password123');

        $this->assertFalse($result, "Authentication should fail for unknown user.");
    }

    // Test pour vérifier le cas d'un mot de passe incorrect
    public function testAuthenticateUserIncorrectPassword()
    {
        $this->stmt->method('fetch')->willReturn([
            'User_ID' => '2',
            'User_Pseudo' => 'anotheruser',
            'User_MDP' => 'correctpassword'
        ]);

        $result = authenticateUser($this->pdo, 'anotheruser', 'wrongpassword');

        $this->assertFalse($result, "Authentication should fail for incorrect password.");
    }
}
