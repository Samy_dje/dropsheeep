<?php
// Inclusion directe des paramètres de connexion à la base de données dans le new PDO
$bdd = new PDO('mysql:host=localhost;dbname=dropsheep;charset=utf8;', 'root', 'root');

// Récupération des données envoyées via un formulaire POST
$pseudo = $_POST['pseudo'];
$mdp = $_POST['mdp'];
$bio = $_POST['bio'];
$adresse = $_POST['adresse'];
$role = $_POST['role'];

// Vérification pour s'assurer que tous les champs sont remplis
if (empty($pseudo) || empty($mdp) || empty($bio) || empty($adresse) || empty($role)) {
    echo "Tous les champs doivent être remplis.";
} else {
    // Préparation de la requête SQL pour insérer les données dans la base

    $query = "INSERT INTO pj_user (User_Pseudo, User_MDP, User_Bio, User_Adresse, User_Role) VALUES (:pseudo, :mdp, :bio, :adresse, :role)";

    // Préparation de la requête
    $stmt = $bdd->prepare($query);

    if ($stmt) {
        // Liaison des paramètres de la requête avec les valeurs reçues du formulaire
        $stmt->bindParam(':pseudo', $pseudo);
        $stmt->bindParam(':mdp', $mdp);
        $stmt->bindParam(':bio', $bio);
        $stmt->bindParam(':adresse', $adresse);
        $stmt->bindParam(':role', $role);

        // Exécution de la requête et vérification de sa réussite
        if ($stmt->execute()) {
            echo "Inscription réussie !";
            header("Location: index.php"); // Redirection vers une autre page en cas de succès
        } else {
            echo "Erreur lors de l'inscription : " . $stmt->errorInfo()[2];
        }
        // Fermeture du statement
        $stmt->closeCursor();
    } else {
        echo "Erreur lors de la préparation de la requête : " . $bdd->errorInfo()[2];
    }
}

?>


