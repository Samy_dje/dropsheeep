<?php
// Récupération des informations de connexion à partir des variables d'environnement
$host = getenv('DB_HOST') ?: 'localhost';
$dbname = getenv('MYSQL_DATABASE') ?: 'dropsheep';
$username = getenv('MYSQL_USER') ?: 'root';
$password = getenv('MYSQL_PASSWORD') ?: 'root';


try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    // Définit le mode d'erreur de PDO sur Exception pour une meilleure gestion des erreurs
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $e) {
    // Gestion de l'erreur de connexion et affichage d'un message
    echo "Connection failed: " . $e->getMessage();
}

try {
    $stmt = $conn->prepare("SELECT * FROM products"); // Préparation de la requête SQL
    $stmt->execute(); // Exécution de la requête

    // Récupération des résultats sous forme d'un tableau associatif
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Configuration de l'en-tête HTTP pour indiquer un contenu de type JSON
    header('Content-Type: application/json');
    // Envoi des données au format JSON
    echo json_encode($result);
    // Fermeture de la connexion à la base de données
    $conn = null;

} catch (PDOException $e) {
    // Gestion des erreurs lors de l'exécution de la requête
    echo "Error: " . $e->getMessage();
}