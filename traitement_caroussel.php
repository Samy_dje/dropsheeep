<?php
// Récupération des informations de connexion à partir des variables d'environnement
$host = getenv('DB_HOST') ?: 'localhost';
$dbname = getenv('MYSQL_DATABASE') ?: 'dropsheep';
$username = getenv('MYSQL_USER') ?: 'root';
$password = getenv('MYSQL_PASSWORD') ?: 'root';

// Connexion à la base de données avec PDO
try {
    $bdd = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);

    // Définition du mode d'erreur PDO sur Exception pour une meilleure gestion des erreurs

    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Requête SQL pour obtenir les produits en stock
    $sql = "SELECT id ,nom_produits, description, prix, image, en_stock FROM products";
    $stmt = $bdd->query($sql);

    // Parcours des produits et affichage sous forme de cartes HTML
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        // Vérification si le produit est en stock
        if ($row["en_stock"] > 0) {
            // Génération de la structure HTML pour chaque produit
            echo "<div class=\"col-md-4 col-lg-3 mb-4\">";
            echo "<div class=\"card\" data-id=\"" . htmlspecialchars($row["id"]) . "\" data-bg-img=\"" . htmlspecialchars($row["image"]) . "\">";
            echo "<img src=\"" . htmlspecialchars($row["image"]) . "\" class=\"card-img-top\" alt=\"Product Image\">";
            echo "<div class=\"card-body\">";
            echo "<h5 class=\"card-title\">" . htmlspecialchars($row["nom_produits"]) . "</h5>";
            echo "<p class=\"card-text\">" . htmlspecialchars($row["description"]) . "</p>";
            echo "<div class=\"product_price\">";
            echo "<span class=\"price\">" . htmlspecialchars($row["prix"]) . "€</span>";
            echo "</div>";
            echo "<a class=\"btn btn-primary\">Acheter</a>";
            echo "</div>";
            echo "</div>";
            echo "</div>";
        }
    }
} catch (PDOException $e) {
    // Affichage d'un message d'erreur en cas de problème de connexion à la base de données
    echo "Erreur de connexion à la base de données: " . $e->getMessage();
}
?>
