<?php
// Utilise les variables d'environnement pour obtenir les informations de connexion à la base de données
$host = getenv('DB_HOST') ?: 'localhost';
$dbname = getenv('MYSQL_DATABASE') ?: 'dropsheep';
$username = getenv('MYSQL_USER') ?: 'root';
$password = getenv('MYSQL_PASSWORD') ?: 'root';

// Connexion à la base de données MySQL avec PDO
$bdd = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);

// Function to authenticate user
function authenticateUser($bdd, $pseudo, $mdp)
{
    // Préparation et exécution de la requête SQL pour trouver l'utilisateur par son pseudo

    $query = "SELECT User_ID, User_Pseudo, User_MDP FROM pj_user WHERE User_Pseudo = :pseudo";
    $stmt = $bdd->prepare($query);

    if (!$stmt) {
        echo 'Erreur lors de la préparation de la requête : ' . $bdd->errorInfo()[2];
        return false;
    }

    $stmt->bindParam(':pseudo', $pseudo);

    if (!$stmt->execute()) {
        echo "Erreur lors de l'exécution de la requête : " . $stmt->errorInfo()[2];
        return false;
    }

    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $stmt->closeCursor();

    // Vérification si le mot de passe fourni correspond à celui dans la base de données

    if (!$result) {
        echo 'Pseudo introuvable.';
        return false;
    }

    if ($mdp != $result['User_MDP']) {
        echo 'Mot de passe incorrect.';
        return false;
    }

    return $result;
}

// Gère la requête POST pour la connexion
// Authentifie l'utilisateur et démarre une session si les informations sont correctes
// Redirige vers une autre page en fonction du succès ou de l'échec de la connexion
function handleLogin($bdd)
{
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $pseudo = $_POST['pseudo'];
        $mdp = $_POST['mdp'];

        $user = authenticateUser($bdd, $pseudo, $mdp);
        if ($user) {
            session_start();
            $_SESSION['User_ID'] = $user['User_ID'];
            $_SESSION['User_Pseudo'] = $user['User_Pseudo'];
            header('Location: index.php');
            exit();
        } else {
            header('Location: connexion.php');
        }
    }
}

// Appelle la fonction handleLogin pour traiter la tentative de connexion de l'utilisateur
handleLogin($bdd);